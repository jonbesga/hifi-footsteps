HIFI Footsteps
========
Learning C++ while reading the commits of [High Fidelity](https://github.com/highfidelity/hifi/commits/).

To check the progress look at the [CHANGELOG](CHANGELOG.md)

IMPLEMENTING NETWORK SOCKETS
--------------------

Currently the program try to send and receive a message in the same machine. Also, there is a `raspberry.cpp` that can be compiled and executed in another device to try the reception of messages over a private network (by default: 192.168.1.100).

IMPLEMENTING SERIAL PORT CONNECTION AND READING
-----------------------------

The program open the serial port at the specifid location and read the data. However, the first lectures have 'junk' and sometimes data is not fully transmited (missing some numbers). I suppose this has to do with the way I'm reading the serial port and the options set for it.

IMPLEMENTING AUDIO (PORT AUDIO)
-----------------------------------

Using the Port Audio library, I've implemented an audio interface. Now it's just produces noise.


BUILDING
------------

Use the Makefile.

`make`
