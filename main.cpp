#include <string>
#include <iostream>
#include "network.h"
#include "serial.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

int socketfd, serialfd;
using namespace std;

void test_network(){
	// Create socket and choose UDP because it's faster than TPC and we don't need too much reliability, ie., we can afford
	// to lose some packets.
	// Passing 0 to create_socket() will create a non blocking socket.
	socketfd = create_socket(0);
	if (socketfd){
		cout << "Socket created and bound successfully (FD: " << socketfd << ")." << endl;
	
		// Test network connection in two ways.
		string message = "This is a message prepared to be received by the sender machine!";
		
		// Using as receiver localhost
		string destination_ip = "127.0.0.1";
		network_send(socketfd, message, message.size(), destination_ip);
		
		string source_ip = "127.0.0.1";
		network_receive(socketfd, source_ip);
		// Using as receiver a device (e.g. a RaspberryPi) located at 192.168.1.100 and listening for a message.
		message = "This is a message prepared to be received by another device!";
		destination_ip = "192.168.1.100";
		network_send(socketfd, message, message.size(), destination_ip);
	}
}

void test_serial(){
	serialfd = open_serial("/dev/ttyACM0");
	if(serialfd){
		cout << "Device found. Serial port opened successfully." << endl;
	
		configure_serial(&serialfd, 115200);

		while(1){
			read_serial(serialfd);
		}
	}
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode){
	// When a user presses the escape key, we set the WindowShouldClose property to true, 
	// closing the application
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

int main(){
//	test_network();
//	test_serial();
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);	
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);	
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	GLFWwindow* window = glfwCreateWindow(800,600, "Main", nullptr, nullptr);
	if (window == nullptr){
		cout << "Failed to create GLFW window" << endl;
		glfwTerminate();
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK){
		cout << "Failed to initialize GLEW" << endl;
		return -1;
	}


	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
  
	glViewport(0, 0, width, height);
	glfwSetKeyCallback(window, key_callback);

	GLfloat vertices[] = {
	    -0.5f, -0.5f, 0.0f,
	     0.5f, -0.5f, 0.0f,
	     0.0f,  0.5f, 0.0f
	};

	GLuint VBO;
	glGenBuffers(1, &VBO); 

	glBindBuffer(GL_ARRAY_BUFFER, VBO);  

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	const GLchar* vertexShaderSource = "#version 330 core\n"
	    "layout (location = 0) in vec3 position;\n"
	    "void main()\n"
	    "{\n"
	    "gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
	    "}\0";
	const GLchar* fragmentShaderSource = "#version 330 core\n"
	    "out vec4 color;\n"
	    "void main()\n"
	    "{\n"
	    "color = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
	    "}\n\0";

	GLuint vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	GLuint fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	GLuint shaderProgram;
	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);  

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);  

	GLuint VAO;
	glGenVertexArrays(1, &VAO);

	   glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

	    glBindVertexArray(0); // Unbind VAO 
	while(!glfwWindowShouldClose(window)){
		glfwPollEvents();
		
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindVertexArray(0);  
		
		glfwSwapBuffers(window);
	}
	
	glfwTerminate();
	return 0;
}

