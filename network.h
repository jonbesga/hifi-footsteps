// network.h
//
// Created by Jon Ander Besga on 9/5/2016
//

#ifndef network_interface_h
#define network_interface_h

#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <iostream>

using namespace std;

const int UDP_PORT = 30000;
const int MAX_PACKET_SIZE = 65535; 

int create_socket(int blocking);
int network_send(int socketfd, string packet_data, int packet_size, string destination_ip);
int network_receive(int socketfd, string source_ip);

#endif
