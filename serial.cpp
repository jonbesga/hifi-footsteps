#include "serial.h"
#include <string.h>
#include <termios.h>

const int MAX_BUFFER = 256;
char serial_buffer[MAX_BUFFER];
int serial_buffer_pos = 0;
int sensor_data[4];


void configure_serial(int *fd, int baud)
{
    struct termios options;
    tcgetattr(*fd, &options);

    switch(baud)
    {
		case 9600:
			cfsetispeed(&options,B9600);
			cfsetospeed(&options,B9600);
			break;
		case 19200:
			cfsetispeed(&options,B19200);
			cfsetospeed(&options,B19200);
			break;
		case 38400:
			cfsetispeed(&options,B38400);
			cfsetospeed(&options,B38400);
			break;
		case 115200:
			cfsetispeed(&options,B115200);
			cfsetospeed(&options,B115200);
			break;
		default:
			cfsetispeed(&options,B9600);
			cfsetospeed(&options,B9600);
			break;
    }

    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
    tcsetattr(*fd,TCSANOW,&options);
}


int open_serial(string location){
	// O_RDWR	Open for reading and writing. The result is undefined if this flag is applied to a FIFO.
	// O_NOCTTY	The O_NOCTTY flag tells UNIX that this program doesn't want to be the "controlling terminal" for that port. If you don't specify this then any input (such as keyboard abort signals and so forth) will affect your process. Programs like getty(1M/8) use this feature when starting the login process, but normally a user program does not want this behavior.	
	// O_NDELAY	The O_NDELAY flag tells UNIX that this program doesn't care what state the DCD signal line is in - whether the other end of the port is up and running. If you do not specify this flag, your process will be put to sleep until the DCD signal line is the space voltage. Returns 0!?
	// O_NONBLOCK	Returns -1
	int serialfd = open(location.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK); //| O_NDELAY);

	if(serialfd == -1){
		perror("Unable to open serial port");
		return false;
	}
	return serialfd;
}


// TODO: O_NONBLOCK SOMETIMES shows weird symbols
void read_serial(int serialfd){
	char tmp_buffer[256];
	memset(tmp_buffer, '\0', sizeof(tmp_buffer));
	int bytes = read(serialfd, tmp_buffer, sizeof(tmp_buffer));
	if(bytes > 0){
		cout << tmp_buffer;
	}
}

