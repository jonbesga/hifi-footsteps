#ifndef serial_interface_h
#define serial_interface_h

#include <iostream>
#include <unistd.h> // To read serial
#include <fcntl.h> // To open serial
#include <string>

using namespace std;


int open_serial(string location);
void configure_serial(int *serialfd,int baud);
void read_serial(int serialfd);

#endif
