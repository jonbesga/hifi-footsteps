// network.cpp
//
// Created by Jon Ander Besga on 9/5/2016
//


#include "network.h"

sockaddr_in address, dest_address, source_address;

int create_socket(int blocking){
	// Create socket
	// AF_INET	IPv4 Internet protocols
	// SOCK_DGRAM	Supports datagrams (connectionless, unreliable messages of a fixed maximum length).
	// PROTOCOL	Specifying a protocol of 0 causes socket() to use an unspecified default protocol appropriate for the requested socket type 
	// SOCK_NONBLOCK	Non-blocking socket. More at: http://www.linuxhowtos.org/manpages/2/socket.htm
	
	int socketfd = -1;
	if(!blocking){
		socketfd = socket( AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0);
	}
	else{
		socketfd = socket( AF_INET, SOCK_DGRAM, 0);
	}
	if(socketfd <= 0){
		cout << "Failed to create socket." << endl;
		return false;
	}

	address.sin_family = AF_INET;
	// INADDR_ANY	Allowed your program to work without knowing the IP address of the machine it is running on, or, in the case of a machine with multiple network interfaces, it allowed your server to receive packets destined to any of the interfaces.
	address.sin_addr.s_addr = INADDR_ANY;
	// HTONS	The htonl() and htons() functions shall return the argument value converted from host to network byte order. More at: https://gnunet.org/book/export/html/363.
	address.sin_port = htons(UDP_PORT);

	// In memory, the struct sockaddr_in is the same size as struct sockaddr, and you can freely cast the pointer of one type to the other without any harm, except the possible end of the universe.
	// More at: http://www.gta.ufrj.br/ensino/eel878/sockets/sockaddr_inman.html
	int res = bind(socketfd, (sockaddr*)&address, sizeof(sockaddr_in));
	if(res < 0){
		cout << "Failed to bind socket." << endl;
		return false;
	}
	return socketfd;
}

int network_send(int socketfd, string packet_data, int packet_size, string destination_ip){
	dest_address.sin_family = AF_INET;
	// inet_addr 	Converts a string containing an IPv4 dotted-decimal address into a proper address for the IN_ADDR structure
	dest_address.sin_addr.s_addr = inet_addr(destination_ip.c_str());
	dest_address.sin_port = htons(UDP_PORT);

	// ssize_t sendto(int sockfd, const void *buf, size_t len, int flags,
	//                const struct sockaddr *dest_addr, socklen_t addrlen);
	int bytes_sent = sendto(socketfd, packet_data.c_str(), packet_size, 0, (sockaddr*) &dest_address, sizeof(sockaddr_in));

	if(bytes_sent != packet_size){
		cout << "Failed to send packet: " << strerror(errno) << endl;
		return false;
	}
	else{
		cout << "Sent: " << packet_data << "(" << bytes_sent << " bytes sent to " << destination_ip << ":" << UDP_PORT << ")" << endl; 
	}
	return bytes_sent;

}

int network_receive(int socketfd, string source_ip){
	source_address.sin_family = AF_INET;
	source_address.sin_addr.s_addr = inet_addr(source_ip.c_str());
	source_address.sin_port = htons(UDP_PORT);

	char buffer[MAX_PACKET_SIZE];
	socklen_t size_addr_from = sizeof(&source_address);
	// ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
	//		                 struct sockaddr *src_addr, socklen_t *addrlen);
	int received_bytes = recvfrom(socketfd, buffer, MAX_PACKET_SIZE, 0, (sockaddr*) &dest_address, &size_addr_from);
	
	if(received_bytes < 0){
		cout << "Failed to receive packet: " << strerror(errno) << endl;
		return false;
	}
	cout << "Received: " << buffer << "(" << received_bytes << " bytes received)" << endl;
	return received_bytes;
}
