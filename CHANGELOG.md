# CHANGELOG

The following changelog shows my progress through the [HIFI repository](https://github.com/highfidelity/hifi/). My code is updated until the top commit of this list.

The commits linked here are just for personal reference.

## COMMITS HISTORY

[Merge branch 'master' of https://github.com/worklist/interface](https://github.com/highfidelity/hifi/commit/610948cc18e619ee79292988e055939f69fc1cd7)
[Adding Makefile for linux](https://github.com/highfidelity/hifi/commit/94ef974635d840fc3b54b2ce1cd287217aa98a60)
[Wrapping includes with #ifdef for linux compatibility](https://github.com/highfidelity/hifi/commit/9b8e8ff5ded12e48628f7c5ec7defd4a0fb551a1)
[Added audio code](https://github.com/highfidelity/hifi/commit/933946320a90f5a6f04870ca28cf98a4e81fe5bf)
	- [Added audio docs](https://github.com/highfidelity/hifi/commit/094faf36179fb643fff360cb03ae202c6dd7cd14)
	- [Minor changes: added an additional audio buffer (captures audio input](https://github.com/highfidelity/hifi/commit/908a8532230fa62582fcf95f941facb5178350eb)
[Initial commit](https://github.com/highfidelity/hifi/commit/092fb77a98e874eb8e6087eef585e818796fab0d)
